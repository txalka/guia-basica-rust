fn main() {
    // Flujos de control
    
    let mut x: Option<u8> = Some(20);
    //x = None;
    //match x {
    //    Some(i) => println!("{}", i),
    //    _ => println!("No hay valor")
    //}

    if let Some(i) = x {
        println!("{}", i);
    }  
    //else if x == 1 {

    //}
    else {
        println!("No hay valor (if let)");
    }


    while let Some(i) = x
    {
        if i == 0
        {
            x = None;
        } else
        {
            println!("En While {}", i);
            x = Some(i-1);
        }
    }

    // Ciclo for


    let arr = [10, 20, 30, 40, 50];

    for element in arr.iter() {
        println!("En for el valor es: {}", element);
    }

    for number in 1..11 {      // Iterar en rango determinado, del 1 al 10
        println!("En for iterando del 1 al {}", number);
    }

    for number in (1..11).rev() {      // Iterar de mayor a menor en rango determinado, del 1 al 10
        println!("En for iterando de mayor a menor del 1 al {}", number);
    }
}
