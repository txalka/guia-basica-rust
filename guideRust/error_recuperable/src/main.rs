use std::fs::File;
use std::io;
use std::io::Read;
use std::io::ErrorKind;


fn main() {
    let f = File::open("f.txt").unwrap();  //si la operación es exitosa devuelve el valor de la variable OK, su falla un panic!
    let f2 = File::open("f2.txt").expect("Ups!!"); //con expect si la operación  falla podemos generar el panic!
    println!("{:?}", f);
    println!("--------------------------");
    println!("{:?}", f2);
    println!("--------------------------");
    let f3 = read_file();
    println!("{:?}", f3);
}

fn read_file() -> Result<String, io::Error> {
    //let mut f = match File::open("f.txt") {
    //    Ok(f) => f,
    //    Err(e) => return Err(e),
    //};
    let mut f = File::open("f.txt")?;  // ? crea el match, si es exitoso retorna en f, si es error lo retorno a la salida con io <String, io::Error>
    let mut s = String::new();
    //match f.read_to_string(&mut s) {
    //    Ok(_) => Ok(s),
    //    Err(e) => return Err(e),
    //}
    f.read_to_string(&mut s)?;
    Ok(s) //retorna s --- los retornos son sin ;

}

fn formaPocoOptima() {
    let f = File::open("f.txt");
    let f = match f {
        Ok(file) => file,
        Err(err) => match err.kind() { //busca tipo problema
            ErrorKind::NotFound => match File::create("f.txt"){ //si hay un problema ejecuta una accion
                Ok(file_created) => file_created,
                Err(err2) => panic!("{:?}", err2), //en caso de no poder ejecutar nada
            },
            _ => panic!("Hubo un problema")
        },
    };
    println!("{:?}", f);
}
