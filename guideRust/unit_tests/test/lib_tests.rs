// Se crea modulo

#[cfg(test)] // que compile solo con cargo test.
mod tests {
    #[cfg(test)]
    mod tests {
        #[test]
        fn it_works() {
            assert_eq!(2 + 2, 4);
        }
    }
    
    #[test]
    fn test_hola_espaniol() {
        assert_eq!("hola", saludos::esp::hola());
    }
    
    #[test]
    fn test_hola_frances() {
        assert_eq!("salut", saludos::frn::hola());
    }
    
    #[test]
    fn test_chao_espaniol() {
        assert_eq!("chao", saludos::esp::chao());
    }
    
    #[test]
    #[should_panic]
    fn test_fallar() {
        assert_eq!("chaooooo", saludos::esp::chao());
    }
}

