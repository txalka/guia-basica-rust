fn main() {
    let mut i = 1;
    while i <= 100 {
        i <<= 1;
        print!("{} ", i);
    }
    
    let mut i = 1;
    // 1..50
    // i % 3 != 0
    // i*i < 400
    while i <= 50 {
        i += 1;
        if i % 3 == 0 { continue; }
        if i * i >= 400 { break; }
        println!("{}", i * i);
    }
    
    let mut i = 1;
    loop { // = while true
        let x = i * i;
        if x >= 200 { break; }
        println!("{}", x);
        i += 1;
    }
}

