struct Gato {
    nombre: String,
}

impl Gato {
    fn new() -> Self {
        Self {
            nombre: "Minino".to_string(),
        }
    }

    fn hace(&self) -> String {
        format!("{} hace miau", self.nombre)
    }
}

fn main() {
    let gato = Gato::new();
    println!("{}", gato.hace());
}

