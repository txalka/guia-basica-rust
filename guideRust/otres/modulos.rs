mod animal {
    fn nadar() {}
    pub mod mamifero {
        pub fn parir() {}
        pub fn amamantar() {}
        pub fn correr() {}
        pub fn comer() {
            correr();
            crate::animal::nadar();
            super::nadar();
            beber::consumir_agua();
        }
        pub mod beber {
            pub use crate::plantas::enredaderas::consumir_agua;
        }
    }
    pub mod ave {
        pub fn desovar() {}
    }
    pub mod anfibio {
        pub fn desovar() {}
    }
}

use animal::mamifero::parir;
use animal::mamifero::amamantar;
use animal::anfibio::desovar as anf_desovar;
use animal::ave::desovar;

fn osa_pare_amamanta() {
    parir(); //ruta relativa
    amamantar();
}

fn tortuga_en_la_playa() {
    anf_desovar();
}

fn sembrar() {
    let mut arbol = plantas::Arbol::new();
    arbol.numero_hojas = 1000000;
}

mod plantas {
    pub struct Arbol {
        pub numero_hojas: i64,
        es_frutal: bool,
    }
    impl Arbol {
        pub fn new() -> Self {
            Arbol {
                numero_hojas: 0,
                es_frutal: false,
            }
        }
    }
    pub mod enredaderas {
        pub fn consumir_agua() {}
    }
}

