fn main() {
    let numero = 10;
    match numero {
        1 => println!("uno"),
        2 => println!("dos"),
        3 => println!("tres"),
        4 | 5 => println!("cuatro o cinco"),
        6..=10 => println!("entre seis y diez"),
        _ => println!("desconocido"),
    }
    
    let nombre = "francisco";
    match nombre {
        "pedro" => println!("hola Pedrito"),
        "juan" => println!("hola Juancito"),
        "francisco" => println!("hola pancho"),
        _ => println!("no te reconozco"),
    }
}

