fn main() {
    let n = -7;
    let a = if n > 0 { true } else { false };
    
    if n > 0 {
        println!("El número es positivo");
    } else if n == 0 {
        println!("El número es CERO");
    } else {
        println!("El número es negativo");
    }
    
    println!("{}",
        if n > 0 {
            "El número es positivo"
        } else if n == 0 {
            "El número es CERO"
        } else {
            "El número es negativo"
        }
    );
    println!("a = {}", a);
}

