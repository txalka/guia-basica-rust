use std::thread;
use std::time::Duration;
use std::sync::mpsc;

fn main() {
    let mut n = 10;
    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        let v = vec![1,2,3,4,5];
        for i in v {
            n += i;
            tx.send(n).unwrap();
            thread::sleep(Duration::from_millis(100));
        }
    });
    for r in rx {
        println!("Mensaje desde tx: {}", r);
    }
}

