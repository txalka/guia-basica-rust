fn compara() {
    let a = 10;
    let b = Box::new(10);   //dirección de memoria que apunta al valor. 
                            //La box tiene un tamaño conocido para el compilador
    if a == *b {   //*b como referencia
        println!("Valores ");
    }
}

//Box<T>
enum Lista {
    Nodo(i32, Box<Lista>),
    None,
}
use Lista::*;

fn main() {
    let lista = Nodo(2, Box:: new (Nodo(4, Box:: new (Nodo(7, Box:: new (None))))));
    sombreadoVariables();
}

// SOmbreado de variables
fn sombreadoVariables(){
    let x = 10; // valor inicial
    println!("{}", x);
    let x = "10"; //Se sombre variable x con otro tipo de datos
    println!("{}", x);
}



