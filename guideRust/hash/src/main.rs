use crypto_hash::{Algorithm, hex_digest};


fn main() {
    let digest = hex_digest(Algorithm::SHA256, b"mateo");
    println!("Mateo cifrado en SHA256: {}" , digest);
}
