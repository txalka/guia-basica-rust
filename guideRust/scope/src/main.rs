fn main() {
    //scope main 1 .... " { } " llaves 1 scope
    let a = 1;
    {
        //anidando llaves se añade otro scope
        let a = 22;
        println!("{ }", a);
        if true{
            let b = "let b";
            println!("{ }", b);
        }
        
    }//muere a 
    println!("{ }", a);
    println!("{}", scope_1());
}//muere a


// Otro ejemplo 
fn scope_1() -> i32 {
    let x = 2;
    println!("X de función scope_1:  {}", x);
    // Ejemplo de operaciones dentro de una variable usando un scope
    let y = {
        let x = 5; // Es otra variable x. Vemos como se comporta el scope
        println!("X dentro de scope de Y: {}", x);
        x * 2 
    };// Acá muere la x de y
    
    let resultado : i32 = x + y;
    println!("X terminando: {}", x);
    println!("valor final que retorna función scope_1: {}", resultado);
    return resultado;
}
