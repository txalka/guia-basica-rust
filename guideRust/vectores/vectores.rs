fn main() {
    let mut v: Vec<Tipo> = Vec::new(); //necesita saber que tipo de datos usa vector, para compilador
    #[derive(Debug)]
    enum Tipo {
        Entero(i32),
        Decimal(f64),
        Texto(String),
    }
    v.push(Tipo::Texto(String::from("hola")));
    v.push(Tipo::Decimal(2.3));
    v.push(Tipo::Entero(333));
    v.push(Tipo::Entero(1));
    println!("{:?}", v);
}

