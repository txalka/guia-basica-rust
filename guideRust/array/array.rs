// Array deben tener el mismo valor

fn main() {
    let a = [34, 23, 12];
    println!("primer elemento de a = {}", a[0]);
    
    let a: [i8; 5] = [0; 5]; // Declaramos tipo de datos y cantidad de datos que tendrá el arreglo
    let a_1 = a[1];          // Acceder a elemeto en dicha posición 
    println!("{:?}", a);     // {:?} imprime estructura
    
    let mut a = [0i64; 5];  // 5 elementos de 0
    let elem = a.get(22);   
    println!("{:?} de {} bytes", a, std::mem::size_of_val(&a));
    
    let mut a = [0i64; 5];
    println!("tamano {}", a.len());
    
    let a = [3, 5, 2, 5];
    for i in 0..a.len() {
        println!("indice: {} elemento {}", i, a[i]);
    }
    let matriz:[[u8; 4]; 3] = [
        [3,4,5,6],
        [4,9,1,2],
        [2,1,5,7],
    ];
    println!("{}", std::mem::size_of_val(&matriz));
    println!("{:?}", matriz);
    for i in 0..matriz.len() {
        for j in 0..matriz[i].len() {
            println!("matriz[{}][{}] = {}", i, j, matriz[i][j]);
        }
    }
}

