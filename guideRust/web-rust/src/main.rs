use warp::Filter;   

#[tokio::main]     // macro type of procedure
async fn main() {
    // path! is macro not function. Macro can receive any quantity and type of parameters
    let filter = warp::path!("hello" / String)
    .map(|name| format!("hello {}", name));

    println!("Servidor en el puerto 8081");    // macro declarative

    // receives route and response. Call async
    warp::serve(filter)
    .run( ([0,0,0,0], 8081) )
    .await;
}
