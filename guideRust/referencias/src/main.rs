fn main() {
    let mut s1 = String::from("Referencia");
    let ref1 = &s1;
    let ref2 = &s1;
    println!("{} {}", ref1, ref2); // se dejan ocupar
    let ref3 = &mut s1;
    println!("{}", ref3);
    let s = xy();
}

fn xy() -> String{
    let s = String::from("xx");
    s
}

fn x(){
    let mut x = 10;
    println!("prueba 2 {}", x);
    x = 20;
    println!("prueba 2.1 {}", x);
}
